// Class regroupant tout les pions
class Jeton{
    constructor(couleur){
        if(couleur=="Blanc"){
            this.couleur=0;
        }else{
            this.couleur=1;
        }
    }

    getCouleur(){
        return this.couleur;
    }
};



class Pion extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="pion"
    
    }

    getType(){
        return this.type;
    }

    affichage(){
        //console.log(super.getCouleur());
        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/p.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wp.gif";
            return(img);
        }
    }
};

class Tour extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="tour";
    }

    affichage(){
        
        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/r.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wr.gif";
            return(img);
        }
    }

    getType(){
        return this.type;
    }

}; 

class Cavalier extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="cavalier";
    }

    affichage(){

        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/n.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wn.gif";
            return(img);
        }

    }

    getType(){
        return this.type;
    }

};

class Fou extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="fou";
    }

    affichage(){
        
        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/b.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wb.gif";
            return(img);
        }
    }

    getType(){
        return this.type;
    }

};

class Reine extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="reine";
    
    }

    affichage(){
        //return("Reine " + String(this.getCouleur()));
        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/q.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wq.gif";
            return(img);
        }
    }

    getType(){
        return this.type;
    }

};

class Roi extends Jeton{
    constructor(couleur){
        super(couleur);
        this.type="roi";
    }

    affichage(){
        if(super.getCouleur()==1){

            let img = document.createElement('img');
            img.src = "/assets/k.gif";

            return(img);
        }
        else{
            let img = document.createElement('img');
            img.src = "/assets/wk.gif";
            return(img);
        }
    }

    getType(){
        return this.type;
    }
    
};