class Case{
    constructor(i,j){
        this.i=i;
        this.j=j;
        this.pion=0;
        this.isFree=false;
        this.select=false;
    }

    setPion(elem){
        this.pion=elem;
    }

    getPion(){
        return this.pion;
    }

    hasPion(){
        if(this.pion==0){
            return false
        }
        return true
    }

    setFree(booleen){
        this.isFree=booleen;
    }

    getFree(){
        return this.isFree;
    }

    selected(booleen){
        this.select=booleen;
    }

    getSelected(){
        return this.select;
    }


}

class Plateau{
    constructor(){
        this.plateau=new Array(8);
        for(let i=0;i<8;i++){
            this.plateau[i]=new Array(8);
        }
        this.select;
        this.tour=0;

        this.j0=new Chrono(0);
        this.j1=new Chrono(1);

        this.initialisation();
    }

    //Initialisation du plateau avec des 0 pour les cases vides et des class fille de Jeton pour representer les pions
    initialisation(){
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                this.plateau[i][j]=new Case(i,j);
            }
        }

        //Creation et positionement des pions
        for(let j=0;j<8;j++){
            this.plateau[j][1].pion=new Pion("Blanc");
            this.plateau[j][6].pion=new Pion("Noir");
        }

        //Creation et positionement des tours
        this.plateau[0][0].pion=new Tour("Blanc");
        this.plateau[7][0].pion=new Tour("Blanc");

        this.plateau[0][7].pion=new Tour("Noir");
        this.plateau[7][7].pion=new Tour("Noir");

        //Creation et positionement des cavaliers
        this.plateau[1][0].pion=new Cavalier("Blanc");
        this.plateau[6][0].pion=new Cavalier("Blanc");

        this.plateau[1][7].pion=new Cavalier("Noir");
        this.plateau[6][7].pion=new Cavalier("Noir");

        //Creation et positionement des fous
        this.plateau[2][0].pion=new Fou("Blanc");
        this.plateau[5][0].pion=new Fou("Blanc");

        this.plateau[2][7].pion=new Fou("Noir");
        this.plateau[5][7].pion=new Fou("Noir");

        //Creation et positionement des rois
        this.plateau[4][0].pion=new Roi("Blanc");
        this.plateau[4][7].pion=new Roi("Noir");
        
        //Creation et positionement des Reines
        this.plateau[3][0].pion=new Reine("Blanc");
        this.plateau[3][7].pion=new Reine("Noir");

        this.tour=0;
        this.selected=0;

        document.getElementById('gagnant').textContent="";

        document.getElementById('reinitialiser').style.display="none";

        this.j0.fReset();
        this.j1.fReset();

        this.affichage();

        this.allClick();

    }

    isMovable(event){
        //On recupere les coordonnées
        let coor=event.getAttribute('id').split('');
        let i=parseInt(coor[0]);
        let j=parseInt(coor[1]);

        if(this.plateau[i][j].getFree()){ 
            //Si le deplacement est correct, on bouge le pion
            this.plateau[i][j].setPion(this.select.getPion());
            this.select.setPion(0);
            this.isEchec();
            this.transfoReine();

            if((this.tour+1)%2==0){
                //ca va etre le tour du jouer blanc
                this.j0.fStart()
                this.j1.fStop();
            }else{
                this.j1.fStart();
                this.j0.fStop();
            }

            this.tour++;
        }

        //on reinitialise tout les isFree du jeu
        this.resetFree();

        this.select=this.plateau[i][j];

        if(this.plateau[i][j].hasPion() && this.plateau[i][j].getPion().getCouleur()==this.tour%2){
            //Pion
            if(this.plateau[i][j].getPion().getType()=="pion"){
                this.setFreePion(i,j);
            }

            //cavalier
            else if(this.plateau[i][j].getPion().getType()=="cavalier"){
                this.setFreeCavalier(i,j);
            }

            //tour
            else if(this.plateau[i][j].getPion().getType()=="tour"){
                this.setFreeTour(i,j);
            }

            //Fou
            else if(this.plateau[i][j].getPion().getType()=="fou"){
                this.setFreeFou(i,j);
            }

            //reine
            else if(this.plateau[i][j].getPion().getType()=="reine"){
                this.setFreeTour(i,j);
                this.setFreeFou(i,j);
            }

            //roi
            else if(this.plateau[i][j].getPion().getType()=="roi"){
                this.setFreeRoi(i,j);
            }
        }

        this.affichage();

    }

    setFreePion(i,j){
        //Pion blanc
        if(this.plateau[i][j].getPion().getCouleur()==0){
            if(j+1<8 && this.plateau[i][j+1].getPion()==0){
                this.plateau[i][j+1].setFree(true);
                if(j+2<8 && this.plateau[i][j+2].getPion()==0 && j==1){
                    this.plateau[i][j+2].setFree(true);
                }
            }
            if(i+1<8 && j+1<8 && this.plateau[i+1][j+1].getPion()!=0 && this.plateau[i+1][j+1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i+1][j+1].setFree(true);
            }
            if(i-1>=0 && j+1<8 && this.plateau[i-1][j+1].getPion()!=0 && this.plateau[i-1][j+1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i-1][j+1].setFree(true);
            }
        }

        //Pion Noir
        if(this.plateau[i][j].getPion().getCouleur()==1){
            if(j-1>=0 && this.plateau[i][j-1].getPion()==0){
                this.plateau[i][j-1].setFree(true);
                if(j-2>=0 && this.plateau[i][j-2].getPion()==0 && j==6){
                    this.plateau[i][j-2].setFree(true);
                }
            }
            if(i+1 < 8 && j-1>=0 && this.plateau[i+1][j-1].getPion()!=0 && this.plateau[i+1][j-1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i+1][j-1].setFree(true);
            }
            if(i-1>=0 && j-1>=0 && this.plateau[i-1][j-1].getPion()!=0 && this.plateau[i-1][j-1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i-1][j-1].setFree(true);
            }
        }
    }

    setFreeCavalier(i,j){
        //Mouvement en haut a droite
        if(i+2<8 && j+1<8){
            if (!this.plateau[i+2][j+1].hasPion() || this.plateau[i+2][j+1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i+2][j+1].setFree(true);
            }
        }
        if((j+2)<8 && (i+1)<8){
            if(!this.plateau[i+1][j+2].hasPion() || this.plateau[i+1][j+2].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur()){
                this.plateau[i+1][j+2].setFree(true);
            }
        }

        //Mouvement en bas a droite
        if(i-2>=0 && j+1<8 && (!this.plateau[i-2][j+1].hasPion() || this.plateau[i-2][j+1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i-2][j+1].setFree(true);
        }
        if(i-1>=0 && j+2<8 && (!this.plateau[i-1][j+2].hasPion() || this.plateau[i-1][j+2].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i-1][j+2].setFree(true);
        }

        //Mouvement en haut a gauche
        if(i+2<8 && j-1>=0 && (!this.plateau[i+2][j-1].hasPion() || this.plateau[i+2][j-1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i+2][j-1].setFree(true);
        }
        if(i+1<8 && j-2>=0 && (!this.plateau[i+1][j-2].hasPion() || this.plateau[i+1][j-2].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i+1][j-2].setFree(true);
        }

        //Mouvement en bas a gauche
        if(i-2>=0 && j-1>=0 && (!this.plateau[i-2][j-1].hasPion() || this.plateau[i-2][j-1].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i-2][j-1].setFree(true);
        }
        if(i-1>=0 && j-2>=0 && (!this.plateau[i-1][j-2].hasPion() || this.plateau[i-1][j-2].getPion().getCouleur()!=this.plateau[i][j].getPion().getCouleur())){
            this.plateau[i-1][j-2].setFree(true);
        }
    }

    setFreeFou(i,j){
        let x=1;
        let arret=false;
        //Haut a droite
        while(i+x<8 && j+x<8 && !arret){
            if(this.plateau[i+x][j+x].hasPion()){
                arret=true;
                if(this.plateau[i+x][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j+x].setFree(true);
                }
            }else{
                this.plateau[i+x][j+x].setFree(true);
            }
            x++;
        }
        x=1;
        arret=false;
        //bas a droite
        while(i+x<8 && j-x>=0 && !arret){
            if(this.plateau[i+x][j-x].hasPion()){
                arret=true;
                if(this.plateau[i+x][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j-x].setFree(true);
                }
            }else{
                this.plateau[i+x][j-x].setFree(true);
            }
            x++;
        }

        x=1;
        arret=false;
        //bas a gauche
        while(i-x>=0 && j-x>=0 && !arret){
            if(this.plateau[i-x][j-x].hasPion()){
                arret=true;
                if(this.plateau[i-x][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j-x].setFree(true);
                }
            }else{
                this.plateau[i-x][j-x].setFree(true);
            }
            x++;
        }

        x=1;
        arret=false;
        //haut a gauche
        while(i-x>=0 && j+x<8 && !arret){
            if(this.plateau[i-x][j+x].hasPion()){
                arret=true;
                if(this.plateau[i-x][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j+x].setFree(true);
                }
            }else{
                this.plateau[i-x][j+x].setFree(true);
            }
            x++;
        }
    }

    setFreeTour(i,j){
        //au dessus
        let x=1;
        let arret=false;
        while(j+x<8 && !arret){
            if(this.plateau[i][j+x].hasPion()){
                arret=true;
                if(this.plateau[i][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i][j+x].setFree(true);
                }
            }else{
                this.plateau[i][j+x].setFree(true);
            }
            x++;
        }

        //au bas
        x=1;
        arret=false;
        while(j-x>=0 && !arret){
            if(this.plateau[i][j-x].hasPion()){
                arret=true;
                if(this.plateau[i][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i][j-x].setFree(true);
                }
            }else{
                this.plateau[i][j-x].setFree(true);
            }
            x++;
        }

        //a gauche
        x=1;
        arret=false;
        while(i-x>=0 && !arret){
            if(this.plateau[i-x][j].hasPion()){
                arret=true;
                if(this.plateau[i-x][j].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j].setFree(true);
                }
            }else{
                this.plateau[i-x][j].setFree(true);
            }
            x++;
        }

        //a droite
        x=1;
        arret=false;
        while(i+x<8 && !arret){
            if(this.plateau[i+x][j].hasPion()){
                arret=true;
                if(this.plateau[i+x][j].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j].setFree(true);
                }
            }else{
                this.plateau[i+x][j].setFree(true);
            }
            x++;
        }
    }

    setFreeRoi(i,j){
        //case en haut
        if(j+1<8 && (!this.plateau[i][j+1].hasPion() || this.plateau[i][j+1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i][j+1].setFree(true);
        }

        //Case en bas
        if(j-1>=0 && (!this.plateau[i][j-1].hasPion() || this.plateau[i][j-1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i][j-1].setFree(true);
        }

        //case a droite
        if(i+1<8 && (!this.plateau[i+1][j].hasPion() || this.plateau[i+1][j].getPion().getCouleur() != this.tour%2)){
            this.plateau[i+1][j].setFree(true);
        }

        //Case a gauche
        if(i-1>=0 && (!this.plateau[i-1][j].hasPion() || this.plateau[i-1][j].getPion().getCouleur() != this.tour%2)){
            this.plateau[i-1][j].setFree(true);
        }

        //case en haut a droite
        if(i+1<8 && j+1<8 && (!this.plateau[i+1][j+1].hasPion() || this.plateau[i+1][j+1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i+1][j+1].setFree(true);
        }

        //Case en bas a gauche
        if(i-1>=0 && j-1>=0 && (!this.plateau[i-1][j-1].hasPion() || this.plateau[i-1][j-1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i-1][j-1].setFree(true);
        }

        //case en bas a droite{
        if(j-1>=0 && i+1<8 && (!this.plateau[i+1][j-1].hasPion() || this.plateau[i+1][j-1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i+1][j-1].setFree(true);
        }

        //Case en haut a gauche
        if(j+1<8 && i-1>=0&& (!this.plateau[i-1][j+1].hasPion() || this.plateau[i-1][j+1].getPion().getCouleur() != this.tour%2)){
            this.plateau[i-1][j+1].setFree(true);
        }
    }

    setFreeFou(i,j){
        let x=1;
        let arret=false;
        //Haut a droite
        while(i+x<8 && j+x<8 && !arret){
            if(this.plateau[i+x][j+x].hasPion()){
                arret=true;
                if(this.plateau[i+x][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j+x].setFree(true);
                }
            }else{
                this.plateau[i+x][j+x].setFree(true);
            }
            x++;
        }
        x=1;
        arret=false;
        //bas a droite
        while(i+x<8 && j-x>=0 && !arret){
            if(this.plateau[i+x][j-x].hasPion()){
                arret=true;
                if(this.plateau[i+x][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j-x].setFree(true);
                }
            }else{
                this.plateau[i+x][j-x].setFree(true);
            }
            x++;
        }

        x=1;
        arret=false;
        //bas a gauche
        while(i-x>=0 && j-x>=0 && !arret){
            if(this.plateau[i-x][j-x].hasPion()){
                arret=true;
                if(this.plateau[i-x][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j-x].setFree(true);
                }
            }else{
                this.plateau[i-x][j-x].setFree(true);
            }
            x++;
        }

        x=1;
        arret=false;
        //haut a gauche
        while(i-x>=0 && j+x<8 && !arret){
            if(this.plateau[i-x][j+x].hasPion()){
                arret=true;
                if(this.plateau[i-x][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j+x].setFree(true);
                }
            }else{
                this.plateau[i-x][j+x].setFree(true);
            }
            x++;
        }
    }

    setFreeTour(i,j){
        //au dessus
        let x=1;
        let arret=false;
        while(j+x<8 && !arret){
            if(this.plateau[i][j+x].hasPion()){
                arret=true;
                if(this.plateau[i][j+x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i][j+x].setFree(true);
                }
            }else{
                this.plateau[i][j+x].setFree(true);
            }
            x++;
        }

        //au bas
        x=1;
        arret=false;
        while(j-x>=0 && !arret){
            if(this.plateau[i][j-x].hasPion()){
                arret=true;
                if(this.plateau[i][j-x].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i][j-x].setFree(true);
                }
            }else{
                this.plateau[i][j-x].setFree(true);
            }
            x++;
        }

        //a gauche
        x=1;
        arret=false;
        while(i-x>=0 && !arret){
            if(this.plateau[i-x][j].hasPion()){
                arret=true;
                if(this.plateau[i-x][j].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i-x][j].setFree(true);
                }
            }else{
                this.plateau[i-x][j].setFree(true);
            }
            x++;
        }

        //a droite
        x=1;
        arret=false;
        while(i+x<8 && !arret){
            if(this.plateau[i+x][j].hasPion()){
                arret=true;
                if(this.plateau[i+x][j].getPion().getCouleur()!=this.tour%2){
                    this.plateau[i+x][j].setFree(true);
                }
            }else{
                this.plateau[i+x][j].setFree(true);
            }
            x++;
        }
    }

    resetFree(){
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                this.plateau[i][j].setFree(false);
                this.plateau[i][j].selected(false);
            }
        }
    }

    resetAffichage(){
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                document.getElementById(String(i)+String(j)).textContent="";
            }
        }
    }

    isEchec(){
        //on active tous les pions pour voir le roi adverse est en echec
        let iadv=-1; 
        let jadv=-1;

        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                if(this.plateau[i][j].hasPion()){
                    if(this.plateau[i][j].getPion().getType()=="roi" && this.plateau[i][j].getPion().getCouleur()==(this.tour+1)%2){
                        //on recupere les coordonnées du roi adverse
                        iadv=i;
                        jadv=j;
                    }
                    if(this.plateau[i][j].getPion().getCouleur()==this.tour%2){
                        switch(this.plateau[i][j].getPion().getType()){
                            case "pion":
                                //this.setFreePion(i,j);
                                break;
                            case "cavalier":
                                this.setFreeCavalier(i,j);
                                break;
                            case "tour":
                                this.setFreeTour(i,j);
                                break;
                            case "fou":
                                this.setFreeFou(i,j);
                                break;
                            case "reine":
                                this.setFreeTour(i,j);
                                this.setFreeFou(i,j);
                                break;
                            case "roi":
                                this.setFreeRoi(i,j);
                                break;
                        }
                    }
                }
            }
        }

        let etMat=true;
        let echec = document.getElementById('e');
        let mat = document.getElementById('eem');


        if(iadv!=-1 && jadv!=-1 && this.plateau[iadv][jadv].getFree()){
            console.log("Echec");
            
            echec.style.display = "block";
            mat.style.display="none";

            //Case en haut
            if(jadv+1<8 && (!this.plateau[iadv][jadv+1].hasPion() || this.plateau[iadv][jadv+1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv][jadv+1].getFree()){
                etMat=false;
            }

            //Case en bas
            if(jadv-1>=0 && (!this.plateau[iadv][jadv-1].hasPion() || this.plateau[iadv][jadv-1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv][jadv-1].getFree()){
                etMat=false;
            }

            //Case a droite
            if(iadv+1<8 && (!this.plateau[iadv+1][jadv].hasPion() || this.plateau[iadv+1][jadv].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv+1][jadv].getFree()){
                etMat=false;
            }

            //Case a gauche
            if(iadv-1>=0 && (!this.plateau[iadv-1][jadv].hasPion() || this.plateau[iadv-1][jadv].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv-1][jadv].getFree()){
                etMat=false;
            }

            //case en haut a droite
            if(iadv+1<8 && jadv+1<8 && (!this.plateau[iadv+1][jadv+1].hasPion() || this.plateau[iadv+1][jadv+1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv+1][jadv+1].getFree()){
                etMat=false;
            }

            //Case en bas a gauche
            if(iadv-1>=0 && jadv-1>=0 && (!this.plateau[iadv-1][jadv-1].hasPion() || this.plateau[iadv-1][jadv-1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv-1][jadv-1].getFree()){
                etMat=false;
            }

            //Case en bas a droite{
            if(jadv-1>=0 && iadv+1<8 && (!this.plateau[iadv+1][jadv-1].hasPion() || this.plateau[iadv+1][jadv-1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv+1][jadv-1].getFree()){
                etMat=false;
                console.log("bd");
            }

            //Case en haut a gauche
            if(jadv+1<8 && iadv-1>=0&& (!this.plateau[iadv-1][jadv+1].hasPion() || this.plateau[iadv-1][jadv+1].getPion().getCouleur() != (this.tour+1)%2) && !this.plateau[iadv-1][jadv+1].getFree()){
                etMat=false;
            }
            
            if(etMat){
                console.log("et mat");
                /*var newDiv = document.createElement("div"); // on crée une nouvelle div               ancienne fonction pour afficher echec et mat
                var newText = document.createTextNode("Echec et mat");
                newDiv.appendChild(newText);

                var zone = document.getElementById("eem");
                document.body.insertBefore(newDiv, zone);*/
                
                echec.style.display="none";
                mat.style.display = "block";
            }

        }else{
            echec.style.display = "none";
            mat.style.display="none";
        }

        if(iadv==-1 && jadv==-1){
            //Il n'y a plus de roi adverse
            let p;
            if(this.tour%2==0){
                p="Le joueur Blanc a gagné !"
            }else{
                p="Le joueur Noir a gagné !"
            }

            document.getElementById('gagnant').textContent=p;
            document.getElementById('reinitialiser').style.display="block";
        }
        /*
        let tempsactuel = Date.parse(new Date());
        let tempsenminutes = 10;        //temps du chrono
        let deadline = new Date(tempsactuel + tempsenminutes*60*1000); //*60*1000 pour passer de minutes à milisecondes
        
        if (this.tour==0){
            chrono2div.style.display="block";
            run_chrono2('chrono2div',deadline);
        }
        */
    }

    transfoReine(){
        //ligne du haut
        for(let i=0;i<8;i++){
            if(this.plateau[i][7].hasPion() && this.plateau[i][7].getPion().getType()=="pion"){
                this.plateau[i][7].pion=new Reine("Blanc");
            }
            if(this.plateau[i][0].hasPion() && this.plateau[i][0].getPion().getType()=="pion"){
                this.plateau[i][0].pion=new Reine("Noir");
            }
        }
    }

    allClick(){
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){
                document.getElementById(String(i)+String(j)).addEventListener("click", (function(){
                    this.isMovable(event.currentTarget);
                }).bind(this));
            }
        }
    }

    affichage(){
        this.resetAffichage();
        for(let i=0;i<8;i++){
            for(let j=0;j<8;j++){

                let emplacement=document.getElementById(String(i)+String(j));

                if(this.plateau[i][j].hasPion()){
                    //On ajoute l'image du jeton
                    emplacement.appendChild(this.plateau[i][j].getPion().affichage());
                }

                //Si la case est free, on met un fond vert
                if(this.plateau[i][j].getFree()){
                    emplacement.style.backgroundColor="green";
                    if(this.plateau[i][j].getPion()!=0){
                        emplacement.style.backgroundColor="red";
                    }
                }else{
                    emplacement.style.backgroundColor=null;
                }                    
            }
        }
    }
}
