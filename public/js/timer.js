class Chrono{
   constructor(joueur){
      this.joueur=joueur;
      this.setTm=0;
      this.tmStart=0;
      this.tmNow=0;
      this.tmInterv=0;
      this.tTime=[]; //tableau des temps intermédiaires (lap)
      this.nTime=0; //compteur des temps intermédiaires
      this.setTime();

   }

   //initialisation et creation du timer
   setTime(){
      let j;
      if(this.joueur==0){
         j="blanc";
      }else{
         j="noir";
      }
      //div contenant le chrono
      let div=document.createElement("div");
      div.textContent="Chrono du joueur " + j + " :";

      let timer=document.createElement('p');
      timer.id="timer"+String(this.joueur);
      timer.textContent="00:00:00";

      div.appendChild(timer);
      document.getElementById("chronodiv").appendChild(div);
   }

   affTime(){ //affichage du compteur
      let vMin=this.tmInterv.getMinutes();
      let vSec=this.tmInterv.getSeconds();
      let vMil=Math.round((this.tmInterv.getMilliseconds())/10); //arrondi au centième
      if (vMin<10){
         vMin="0"+vMin;
      }
      if (vSec<10){
         vSec="0"+vSec;
      }
      if (vMil<10){
         vMil="0"+vMil;
      }
      document.getElementById("timer"+String(this.joueur)).innerHTML=vMin+":"+vSec+":"+vMil;
   }

   fChrono(){
      this.tmNow=new Date();
      this.tmInterv=new Date(this.tmNow-this.tmStart);

      this.affTime();
   }

   fStart(){
      this.fStop();
      if (this.tmInterv==0) {
         this.tmStart=new Date();
      } else { //si on repart après un clic sur Stop
         this.tmNow=new Date();
         this.tmStart=new Date(this.tmNow-this.tmInterv);
      }
      this.setTm=setInterval(this.fChrono.bind(this),10); //lancement du chrono tous les centièmes de secondes
   }

   fStop(){
      clearInterval(this.setTm);
      this.tTime[this.nTime]=this.tmInterv;
   }

   fReset(){ //on efface tout
      this.fStop();
      this.tmStart=0;
      this.tmInterv=0;
      this.tTime=[];
      this.nTime=0;
      document.getElementById("timer"+String(this.joueur)).innerHTML="00:00:00";
   }
}