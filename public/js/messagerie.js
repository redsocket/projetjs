function ip_local() //fonction qui permet de prendre la bonne ip selon la connection
                    {
                    var ip = false;
                    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection || false;

                    if (window.RTCPeerConnection)
                    {
                    ip = [];
                    var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};
                    pc.createDataChannel('');
                    pc.createOffer(pc.setLocalDescription.bind(pc), noop);

                    pc.onicecandidate = function(event)
                    {
                    if (event && event.candidate && event.candidate.candidate)
                    {
                        var s = event.candidate.candidate.split('\n');
                        ip.push(s[0].split(' ')[4]);
                    }
                    }
                    }

                    return ip;
                    }
            // Connexion à socket.io


            var socket = io.connect('http://'+ip_local()+':8080');      //créaion d'un socket sur le port 8080
            
            let compteur = 1;       //variable compteur qui stock les nouvelles pages connectés

    
            // On demande le pseudo, on l'envoie au serveur et on l'affiche dans le titre
            
            let pseudo = $('#message').val() ;  //récupere le pseudo rentrer
            
           /* let module = (function(){

                return {printmessage(pseudo){
               
                }
            }
            })
              */
              

            socket.emit('nouveau_client', pseudo);                      //emet quil ya un nouveau client
            document.title = pseudo + ' - ' + document.title;

           

            // Quand on reçoit un message, on l'insère dans la page
            socket.on('message', function(data) {
                insereMessage(data.pseudo, data.message)
                
            })
  
            // Quand un nouveau client se connecte, on affiche l'information
            socket.on('nouveau_client', function(pseudo) { 
                //compteur = compteur + 1;    //chaque page connecté le compteur prend  + 1

                $('#zone_chat').prepend('<p><em>' + pseudo + ' a rejoint le Chat !</em></p>');

                

               /* if(compteur ==3)        //limite le server du jeux a 2 joueurs/2 pages
                {
                    //si il ya un troisème joueur qui tente de se connecter le server ferme indique une erreur
                    document.location.href="html/impossible.html"; 
                    
                    console.log("Vous n'avez pas accés !")
                    
                }

                console.log(compteur);*/
               
            })


        
            
            

            
            // Lorsqu'on envoie le formulaire, on transmet le message et on l'affiche sur la page
            $('#formulaire_chat').submit(function () {
                var message = $('#message').val();
                socket.emit('message', message); // Transmet le message aux autres
                insereMessage(pseudo, message); // Affiche le message aussi sur notre page
                $('#message').val('').focus(); // Vide la zone de Chat et remet le focus dessus
                return false; // Permet de bloquer l'envoi "classique" du formulaire
            });
            
            // Ajoute un message dans la page
            function insereMessage(pseudo, message) {
                $('#zone_chat').prepend('<p><strong>' + pseudo + '</strong> ' + message + '</p>');
            }





